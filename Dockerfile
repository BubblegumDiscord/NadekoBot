FROM microsoft/dotnet:2.0.0-sdk
MAINTAINER Poag <poag@gany.net>

WORKDIR /opt/

#Install required software
#RUN echo "deb http://www.deb-multimedia.org jessie main non-free" | tee /etc/apt/sources.list.d/debian-backports.list \
#	&& apt-get update \
#	&& apt-get install -y --force-yes deb-multimedia-keyring \
#	&& apt-get update \
#	&& apt-get install -y git libopus0 opus-tools libopus-dev libsodium-dev ffmpeg
# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]

RUN apt-get update
RUN apt-get install -y --force-yes redis-server
VOLUME /nadeko
WORKDIR /nadeko
CMD nohup redis-server > /tmp/fuckoff.log & \
    cd /nadeko/src/NadekoBot && \
    dotnet run -c Release
